<?php

/* helper class
*/
class AMV {
  static $file_extension = '.view.php';

  static function list_views() {
    $q = db_query('select * from {views_view} v order by v.human_name asc')->fetchAll();
    return $q;
  }
  
  // returns relative path of saved views folder
  static function views_dir() {
    return conf_path().'/views';
  }
  
  // returns save file name of a view
  static function view_file_name($view) {
    return $view.AMV::$file_extension;
  }
}

